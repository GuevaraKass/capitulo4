print('UNIVERSIDAD NACIONAL DE LOJA ')
print('Ejercicio 2 del capítulo 3')
print('Autora: Kassandra Guevara')
print(' Email: kassandra.guevara@unl.edu.ec')

#Desplaza la última línea del programa anterior hacia arriba, de modo que la lamada a la función aparezca antes que las definiciones.
#Ejecuta el programa y observa qué mensaje de error obtienes.


def muestra_estribillo ()
    print('Verde, verdol')
    print('Endulza la puesta del sol')

def repite_estribillo():
    muestra_estribillo()
    muestra_estribillo()


#Se presentan los mensajes de la función repite estribilloanidada en la función
#Las sentencias de la función muestra estribillo sin llamar a la función.

repite_estribillo()
print('Verde, verdol')
print ('Endulza la puesta del sol')