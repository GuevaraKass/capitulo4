print('UNIVERSIDAD NACIONAL DE LOJA ')
print('Ejercicio 2 del capítulo 3')
print('Autora: Kassandra Guevara')
print(' Email: kassandra.guevara@unl.edu.ec')

# _¿Qué mostrará en pantalla el siguiente programa Python?_

def fred():
    print('Zap')

def jane():
    print('ABC')

jane()
fred()
jane()

# Alternativas, elija la correcta:
# a) Zap ABC  jane fred jane
# b) Zap ABC Zap
# c) ABC Zap ABC
# e) Zap Zap Zap
print('El litral corecto es: ABC Zap ABC')

