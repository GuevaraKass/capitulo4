print('UNIVERSIDAD NACIONAL DE LOJA ')
print('Ejercicio 2 del capítulo 3')
print('Autora: Kassandra Guevara')
print(' Email: kassandra.guevara@unl.edu.ec')

#_ Desplaza la última línea del programa anterior hacia arriba, de modo que la lamada a la función aparezca antes que las definiciones.
# _ Ejecuta el programa y observa qué mensaje de error obtienes.

repite_estribillo()
#no hay declaración de variables, por lo tanto va a ser errror.


def muestra_estribillo():
    print('Verde, verdol')
    print('Endulza la puesta del sol')

def repite_estribillo():
    muestra_estribillo()
    muestra_estribillo()